//
//  ViewController+Extension.swift
//  MyCommitments
//
//  Created by Roberth on 08/10/18.
//  Copyright © 2018 Roberth. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showMessage(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
