//
//  Remember.swift
//  MyCommitments
//
//  Created by User TQI on 06/10/2018.
//  Copyright © 2018 Roberth. All rights reserved.
//

import UIKit
import Firebase

struct Remember {
    
    
    static var saved = [Remember]()
    
    var key: String
    var text: String
    var dateTime: String
    let ref: DatabaseReference?
    
    init(text: String, dateTime: String, key: String) {
        self.text = text
        self.dateTime = dateTime
        self.key = key
        self.ref = nil
    }
    
    func toAnyObject() -> Any {
        return [
            "text": text,
            "dateTime": dateTime
        ]
    }
    
    init(snapshot: DataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        text = snapshotValue["text"] as! String
        dateTime = snapshotValue["dateTime"] as! String
        ref = snapshot.ref
    }
    
}

