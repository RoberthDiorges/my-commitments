//
//  RegisterViewController.swift
//  MyCommitments
//
//  Created by User TQI on 06/10/2018.
//  Copyright © 2018 Roberth. All rights reserved.
//

import UIKit
import UserNotifications
import PKHUD

class RegisterViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var RememberTextField: UITextField!
    @IBOutlet weak var RememberDate: UIDatePicker!
    
    let currentDate = Date()
    private let categoryIdentifier = "udacity.MyCommitments"
    
    struct AlertMessages {
        static let success = "appointment successfully entered!"
        static let unexpectedError = "Unexpected error, please try again later."
        static let invalidDate = "Invalid Date"
        static let invalidField = "Invalid Field!"
        static let requiredTodo = "Required To-do."
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RememberDate.minimumDate = currentDate
        RememberDate.date = currentDate
    }
    
    // MARK: - Actions
    
    @IBAction func doneButton_Clicked(_ sender: UIBarButtonItem) {
        
        guard let title = RememberTextField.text, !title.isEmpty  else {
            showMessage(title: AlertMessages.invalidField, message: AlertMessages.requiredTodo)
            return
        }
        
        guard (RememberDate.date.timeIntervalSinceNow > 0) else {
            showMessage(title: AlertMessages.invalidField, message: AlertMessages.invalidDate)
            return
        }
        
        registerRemember(text: title, date: RememberDate.date)
        
    }
    
    @IBAction func cancelButton_Clicked(_ sender: UIBarButtonItem) {
        clearForm()
    }
    
    // MARK: - Misc
    
    func registerRemember(text: String, date: Date){
        if Reachability.isConnectedToNetwork() {
        let key = FirebaseService.shared.deviceRef.childByAutoId().key
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, MMM dd yyyy hh:mm"
        
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: text, arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: dateFormatter.string(from: date), arguments: nil)
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = categoryIdentifier
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: date.timeIntervalSinceNow, repeats: false)
        let request = UNNotificationRequest(identifier: key, content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(request)
        
        
        HUD.show(.progress)
        
        FirebaseService.shared.save(text: text, date: date, key: key, onSuccess: {
            self.showMessage(title: "Success", message: AlertMessages.success)
        }, onFailure: { (error) in
            self.showMessage(title: "Error", message: AlertMessages.unexpectedError)
        }, onCompleted: {
            HUD.hide()
            self.clearForm()
        })
        } else {
            showMessage(title: "Error", message: "No connection.")
        }
    }
    
    
    func clearForm() {
        RememberTextField.text = ""
        RememberDate.date = currentDate
    }
}

