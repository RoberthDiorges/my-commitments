//
//  RememberTableViewCell.swift
//  MyCommitments
//
//  Created by User TQI on 06/10/2018.
//  Copyright © 2018 Roberth. All rights reserved.
//

import UIKit

class RememberTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setup(Remember: Remember) {
        titleLabel.text = Remember.text
        
        //Format Date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZZZ"
        
        let dateObj = dateFormatter.date(from: Remember.dateTime)
        dateFormatter.dateFormat = "EEE, MMM dd yyyy hh:mm"
        
        dateLabel.text = dateFormatter.string(from: dateObj!)
    }
}

