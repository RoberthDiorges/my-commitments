//
//  RememberTableViewController.swift
//  MyCommitments
//
//  Created by User TQI on 06/10/2018.
//  Copyright © 2018 Roberth. All rights reserved.
//

import UIKit
import UserNotifications
import PKHUD

class RememberTableViewController: UITableViewController {
    
    // MARK: - Life Cycle
    fileprivate func loadCompromisses() {
        if Reachability.isConnectedToNetwork() {
            HUD.show(.progress)
            FirebaseService.shared.load {
                HUD.hide()
                self.tableView.reloadData()
            }
        } else {
            showMessage(title: "Error!", message: "No Connection")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadCompromisses()
    }
    
    // MARK: - UITableView
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Remember.saved.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! RememberTableViewCell
        cell.setup(Remember: Remember.saved[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let alert = UIAlertController(title: "Alert!", message: "Are you sure you want to delete?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                self.deleteItem(indexItem: indexPath.row)
                alert.dismiss(animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    func deleteItem(indexItem index: Int) {
        
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [Remember.saved[index].key])
        
        HUD.show(.progress)
        FirebaseService.shared.remove(ref: Remember.saved[index].ref!, onSuccess: {
            self.showMessage(title: "Success", message: "Deleted successfully.")
            self.tableView.reloadData()
        }, onFailure: { (error) in
            self.showMessage(title: "Error", message: "Could not perform an operation at this time. Please try again later.")
        }, onCompleted: {
            HUD.hide()
        })
        
    }
}
