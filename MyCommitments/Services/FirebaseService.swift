//
//  FirebaseService.swift
//  MyCommitments
//
//  Created by User TQI on 06/10/2018.
//  Copyright © 2018 Roberth. All rights reserved.
//

import UIKit
import Firebase

class FirebaseService {
    
    // MARK: Shared Instance
    static let shared = FirebaseService()
    
    // MARK: - Properties
    private let ref = Database.database().reference(withPath: "Remember-items")
    private let deviceId = UIDevice.current.identifierForVendor!.uuidString
    var deviceRef: DatabaseReference
    
    init() {
        deviceRef = self.ref.child(deviceId.lowercased())
    }
    
    func save(text: String, date: Date, key: String,
              onSuccess: @escaping ()-> Void,
              onFailure: @escaping(_ error: Error) -> Void,
              onCompleted: @escaping ()-> Void){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let itemRef = deviceRef.child(key)
        let item = Remember(text: text, dateTime: "\(date)", key: key)
        
        itemRef.setValue(item.toAnyObject()) { (error, databaseReference) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            if error == nil {
                onSuccess()
            } else {
                onFailure(error!)
            }
            
            onCompleted()
        }
        
    }
    
    func load(completed: @escaping ()-> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        deviceRef.observe(.value, with: { snapshot in
            Remember.saved.removeAll()
            for item in snapshot.children {
                let item = Remember(snapshot: item as! DataSnapshot)
                Remember.saved.append(item)
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            completed()
        })
    }
    
    func remove(ref: DatabaseReference,
                onSuccess: @escaping ()-> Void,
                onFailure: @escaping(_ error: Error) -> Void,
                onCompleted: @escaping ()-> Void) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        ref.removeValue { (error, databaseReference) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if error == nil {
                onSuccess()
            } else {
                onFailure(error!)
            }
            
            onCompleted()
        }
    }
    
}

