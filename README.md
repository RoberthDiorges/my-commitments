# My Commitments

My Commitments
É um aplicativo que vai te ajudar a nunca mais esquecer seus compromissos importantes do dia a dia, com ele é possível criar um lembrete de forma rápida. O aplicativo enviará uma notificação para lembra-lo, tudo muito prático. Todas as notificações são salvas no Firebase e é possível exclui-las quando quiser. Para uma maior segurança, as notificações são salvas usando o Device ID do aparelho, desta forma não é necessário realizar login.

# Instalação
Instalar as dependências no projeto

    $ pod install

# Screenshots

![alt tag](https://bytebucket.org/RoberthDiorges/my-commitments/raw/b3628c8479116c1732c89b9818815dc50b5300e8/MyCommitments/Screens/4.png)

![alt tag](https://bytebucket.org/RoberthDiorges/my-commitments/raw/b3628c8479116c1732c89b9818815dc50b5300e8/MyCommitments/Screens/3.png)

![alt tag](https://bytebucket.org/RoberthDiorges/my-commitments/raw/b3628c8479116c1732c89b9818815dc50b5300e8/MyCommitments/Screens/2.png)

![alt tag](https://bytebucket.org/RoberthDiorges/my-commitments/raw/b3628c8479116c1732c89b9818815dc50b5300e8/MyCommitments/Screens/1.png)

# License
MIT
